package com.bbva.qwai.batch.processor;

import org.springframework.batch.item.ItemProcessor;

public class HelloWorldItemProcessor implements ItemProcessor<String, String> {

	@Override
	public String process(String input) throws Exception {
		String[] message = input.split("\\|");
		return message[0] + " --- " + message[1];
	}

}
